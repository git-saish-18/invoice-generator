import React from "react";
import "../CSS/Preview.css";

const Preview = (props) => {
  let totalEarn = 0;
  let totalDeduce = 0;
  return (
    <div className="main" ref={props.display}>
      <div className="header">
        <h2 className="head Invoicehead " style={{ color: "black" }}>
          Payslip
        </h2>
        <img
          className="previewlogo"
          style={{
            height: "100px",
            width: "100px",
            right: "6rem",
            position: "relative",
          }}
          src="bill.png"
          alt=""
        />
      </div>
      <hr />
      <div className="CompanyMainDetails">
        <div className="companyDetails">
          <h3 className="head">COMPANY SUMMARY</h3>
          <h3>{props.companyDetails.compName} </h3>
          <h5>{props.companyDetails.compAdd} </h5>
          <h5>
            {props.companyDetails.compCity},{props.companyDetails.compstate}{" "}
            {props.companyDetails.comppost}{" "}
          </h5>
          <h5>
            {props.companyDetails.country} , {props.companyDetails.compphno}
          </h5>

          <h5>
            <a href={`https://${props.companyDetails.website}`} target="_blank">
              {props.companyDetails.website}
            </a>
          </h5>
        </div>

        <div className="InvoiceDetails">
          <h4>Invoice Number #{props.billingDetails.invoiceNumber}</h4>
          <h5>Date :- {props.billingDetails.invoicedate}</h5>
          <h5>Due date :- {props.billingDetails.invoicedeudate}</h5>
        </div>
      </div>
      <hr />
      <h3 className="head EMPLOYEE">EMPLOYEE SUMMARY</h3>

      <div className="employeeDetails">
        <div className="employeeChild1">
          <p>
            <b>
              Employee Name : &nbsp;&nbsp;{props.employeeDeatails.employeeName}
            </b>
          </p>
          <p>
            <b>
              Employee Designation : &nbsp;&nbsp;
              {props.employeeDeatails.employeeDesignation}
            </b>
          </p>
          <p>
            <b>
              Employee GrossSalary : &nbsp;&nbsp;₹
              {props.employeeDeatails.employeeGrossSalary}
            </b>
          </p>
        </div>
        <div className="employeeChild2">
          <p>
            <b>
              Employee NetSalary : &nbsp;&nbsp;₹
              {props.employeeDeatails.employeeNetSalary}
            </b>
          </p>
          <p>
            <b>
              Employee WorkDays : &nbsp;&nbsp;
              {props.employeeDeatails.employeeWorkDays}
            </b>
          </p>
          <p>
            <b>
              Employee Absence : &nbsp;&nbsp;
              {props.employeeDeatails.employeeAbsence}
            </b>
          </p>
        </div>
      </div>
      <hr />
      <div className="billing">
        <div>
          <h2 className="head Heading">Earning</h2>
          <div className="mytable">
            <table border={1}>
              <tr>
                <th>Description</th>
                <th>Amount</th>
              </tr>
              {props.Earnings.map((item1, index) => {
                totalEarn += parseInt(item1.unitPrice);

                return (
                  <tr>
                    <td>{item1.description}</td>
                    <td>{item1.unitPrice}</td>
                  </tr>
                );
              })}
            </table>
          </div>
          <div className="totalEarn">
            <span>Total</span>{" "}
            <span>₹{totalEarn === NaN ? "0" : totalEarn}</span>
          </div>
        </div>
        <div>
          <h2 className="head Heading">Deduction</h2>
          <div className="mytable">
            <table border={1}>
              <tr>
                <th>Description</th>
                <th>Amount</th>
              </tr>
              {props.Deduction.map((item1, index) => {
                totalDeduce += parseInt(item1.unitPrice);

                return (
                  <tr>
                    <td>{item1.description}</td>
                    <td>{item1.unitPrice}</td>
                  </tr>
                );
              })}
            </table>
          </div>
          <div className="totalEarn">
            <span>Total</span>{" "}
            <span>₹{totalDeduce === NaN ? "0" : totalDeduce}</span>
          </div>
        </div>
      </div>

      <div className=" TotalPay">
        <div className=" finalhead firstchild">
          <h3>Total Net Payable</h3>
          <p>(Total Earnings- Total Deduction)</p>
        </div>
        <div className="secondchild">
          <p>₹{totalEarn - totalDeduce}</p>
        </div>
      </div>
      <footer>*****</footer>
    </div>
  );
};

export default Preview;
