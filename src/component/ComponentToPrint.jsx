import React, { useState, useRef } from "react";
import ReactToPrint from "react-to-print";
import { useReactToPrint } from "react-to-print";
import "../CSS/ComponentToPrint.css";
import Preview from "./Preview";
const ComponentToPrint = (props) => {
  const [billingDetails, setbillingDetails] = useState({
    invoiceNumber: "",
    invoicedeudate: "",
    invoicedate: "",
  });

  const handleChangebill = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    setbillingDetails((prev) => {
      return { ...prev, [name]: value };
    });
  };

  const [companyDetails, setcompanyDetails] = useState({
    compName: "",
    compAdd: "",
    compCity: "",
    compstate: "",
    comppost: "",
    compphno: "",
    country: "",
    website: " ",
  });

  const handleChangeCmp = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    setcompanyDetails((prev) => {
      return { ...prev, [name]: value };
    });
  };

  const [employeeDeatails, setemployeeDeatails] = useState({
    employeeName: "",
    employeeDesignation: "",
    employeeGrossSalary: "",
    employeeNetSalary: "",
    employeeWorkDays: "",
    employeeAbsence: "",
  });

  const handleChangeEmp = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    setemployeeDeatails((prev) => {
      return { ...prev, [name]: [value] };
    });
  };

  const [preview, setpreview] = useState(false);
  const [previewImg, setpreviewImg] = useState("view.png");

  const [Earnings, setEarnings] = useState([]);
  const [Deduction, setDeduction] = useState([]);

  const addItem = () => {
    setEarnings([...Earnings, { description: "", Amount: 0 }]);
  };
  const addDeduction = () => {
    setDeduction([...Deduction, { description: "", Amount: 0 }]);
  };

  const removeItem = (index) => {
    Earnings.splice(index, 1);
    const updatedEarnings = [...Earnings];
    setEarnings(updatedEarnings);
  };

  const removeDeduction = (index) => {
    Deduction.splice(index, 1);
    const updatedDeduction = [...Deduction];
    setDeduction(updatedDeduction);
  };

  const handleItemChange = (index, field, value) => {
    const updatedEarnings = [...Earnings];
    updatedEarnings[index][field] = value;
    setEarnings(updatedEarnings);
  };

  const handleIDeduction = (index, field, value) => {
    const updatedDeduction = [...Deduction];
    updatedDeduction[index][field] = value;
    setDeduction(updatedDeduction);
  };

  const componentRef = useRef();
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });

  return (
    <div>
      <button
        className="previewbtn"
        onClick={() => {
          preview ? setpreview(false) : setpreview(true);
          preview ? setpreviewImg("view.png") : setpreviewImg("preview.png");
        }}
      >
        <img className="previewImg" src={previewImg} alt="" />
      </button>

      {preview ? (
        <>
          <Preview
            display={componentRef}
            Earnings={Earnings}
            Deduction={Deduction}
            billingDetails={billingDetails}
            companyDetails={companyDetails}
            employeeDeatails={employeeDeatails}
          />

          <ReactToPrint
            trigger={() => <button className="printbtn">Print</button>}
            content={() => componentRef.current}
          />
        </>
      ) : (
        <div className="MainComp">
          <h2 style={{ marginLeft: "0.5rem", color: "blue" }}>Billing Info</h2>
          <div className="invoicedetail">
            <div className="invoicedetailChild">
              <label htmlFor="invoiceNumber">
                Billing Number -
                <input
                  type="text"
                  name="invoiceNumber"
                  id="invoiceNumber"
                  value={billingDetails.invoiceNumber}
                  onChange={handleChangebill}
                />
              </label>
            </div>
            <div className="invoicedetailChild">
              <label htmlFor="invoiceDate">
                Billing Date -
                <input
                  type="date"
                  name="invoicedate"
                  id="invoiceDate"
                  value={billingDetails.invoicedate}
                  onChange={handleChangebill}
                />
              </label>
            </div>
            <div className="invoicedetailChild">
              <label htmlFor="invoiceDueDate">
                Billing Due Date -
                <input
                  type="date"
                  name="invoicedeudate"
                  id="invoiceDueDate"
                  value={billingDetails.invoicedeudate}
                  onChange={handleChangebill}
                />
              </label>
            </div>
          </div>

          <h2
            style={{
              color: "blue",
              fontFamily: "monospace",
              fontFamily: "serif",
              marginTop: "3rem",
            }}
          >
            Company Details{" "}
          </h2>
          <div className="CompanyInfo">
            <div className="billingchild1">
              <div className="BillingInfochild">
                <select name="country" onChange={handleChangeCmp}>
                  <option value="Afghanistan">Afghanistan</option>
                  <option value="Albania">Albania</option>
                  <option value="Bahamas">Bahamas</option>
                  <option value="Bahrain">Bahrain</option>
                  <option value="Burundi">Burundi</option>

                  <option value="Canada">Canada</option>

                  <option value="China">China</option>
                  <option value="Denmark">Denmark</option>

                  <option value="Egypt">Egypt</option>

                  <option value="Finland">Finland</option>
                  <option value="France">France</option>

                  <option value="Germany">Germany</option>
                  <option value="Hong Kong">Hong Kong</option>
                  <option value="Hungary">Hungary</option>
                  <option value="Iceland">Iceland</option>
                  <option value="India">India</option>

                  <option value="Japan">Japan</option>

                  <option value="Kazakhstan">Kazakhstan</option>
                  <option value="Korea">Korea, Republic of</option>

                  <option value="Malaysia">Malaysia</option>

                  <option value="Nepal">Nepal</option>

                  <option value="Oman">Oman</option>
                  <option value="Pakistan">Pakistan</option>

                  <option value="Qatar">Qatar</option>
                  <option value="Reunion">Reunion</option>
                  <option value="Romania">Romania</option>
                  <option value="Russia">Russian Federation</option>
                  <option value="Rwanda">Rwanda</option>
                  <option value="Singapore">Singapore</option>

                  <option value="Thailand">Thailand</option>

                  <option value="Uganda">Uganda</option>
                  <option value="Ukraine">Ukraine</option>
                  <option value="United Arab Emirates">
                    United Arab Emirates
                  </option>
                  <option value="United Kingdom">United Kingdom</option>
                  <option value="United States">United States</option>
                  <option value="Zimbabwe">Zimbabwe</option>
                </select>
              </div>
              <div className="BillingInfochild">
                <input
                  type="text"
                  id=""
                  placeholder="Company Name "
                  name="compName"
                  value={companyDetails.compName}
                  onChange={handleChangeCmp}
                />
              </div>
              <div className="BillingInfochild">
                <input
                  type="text"
                  id=""
                  name="compAdd"
                  placeholder="Address"
                  value={companyDetails.compAdd}
                  onChange={handleChangeCmp}
                />
              </div>
              <div className="BillingInfochild">
                <input
                  type="text"
                  name="compCity"
                  id=""
                  placeholder="City"
                  value={companyDetails.compCity}
                  onChange={handleChangeCmp}
                />
              </div>
            </div>
            <div className="billingchild2">
              <div className="BillingInfochild">
                <input
                  type="text"
                  name="compstate"
                  id="State"
                  placeholder="State"
                  value={companyDetails.compstate}
                  onChange={handleChangeCmp}
                />
              </div>
              <div className="BillingInfochild">
                <input
                  type="website"
                  name="website"
                  id="PhonNum"
                  placeholder="Website"
                  value={companyDetails.website}
                  onChange={handleChangeCmp}
                />
              </div>
              <div className="BillingInfochild">
                <input
                  type="text"
                  name="compphno"
                  id="compphno"
                  placeholder="Phone Number"
                  value={companyDetails.compphno}
                  onChange={handleChangeCmp}
                />
              </div>
              <div className="BillingInfochild">
                <input
                  type="text"
                  name="comppost"
                  id=""
                  placeholder="Postal code/ Zip"
                  value={companyDetails.comppost}
                  onChange={handleChangeCmp}
                />
              </div>
            </div>
          </div>
          <h2
            style={{
              color: "blue",
              fontFamily: "monospace",
              fontFamily: "serif",
              marginTop: "3rem",
            }}
          >
            {" "}
            Employee Details{" "}
          </h2>
          <div className="EmployeeDetails">
            <div className="child1">
              <div className="subchild1">
                <input
                  type="text"
                  name="employeeName"
                  id=""
                  placeholder=" Employee Name "
                  value={employeeDeatails.employeeName}
                  onChange={handleChangeEmp}
                />
              </div>
              <div className="subchild1">
                <input
                  type="number"
                  name="employeeGrossSalary"
                  id=""
                  placeholder="  Gross Salary "
                  value={employeeDeatails.employeeGrossSalary}
                  onChange={handleChangeEmp}
                />
              </div>
              <div className="subchild1">
                <input
                  type="number"
                  name="employeeWorkDays"
                  id=""
                  placeholder="Works Days "
                  value={employeeDeatails.employeeWorkDays}
                  onChange={handleChangeEmp}
                />
              </div>
            </div>
            <div className="child2">
              <div className="subchild2">
                <input
                  type="text"
                  name="employeeDesignation"
                  id=""
                  placeholder=" Designation "
                  value={employeeDeatails.employeeDesignation}
                  onChange={handleChangeEmp}
                />
              </div>
              <div className="subchild2">
                <input
                  type="number"
                  name="employeeNetSalary"
                  id=""
                  placeholder="Net Salary "
                  value={employeeDeatails.employeeNetSalary}
                  onChange={handleChangeEmp}
                />
              </div>
              <div className="subchild2">
                <input
                  type="number"
                  name="employeeAbsence"
                  id=""
                  placeholder="Absence "
                  value={employeeDeatails.employeeAbsence}
                  onChange={handleChangeEmp}
                />
              </div>
            </div>
          </div>
          <div>
            <h2
              style={{
                color: "blue",
                fontFamily: "monospace",
                fontFamily: "serif",
                marginTop: "3rem",
              }}
            >
              Earnings
            </h2>
            <div className="Item">
              <button className="itemaddbtn" onClick={addItem}>
                +
              </button>
              {Earnings.map((item1, index) => (
                <div key={index}>
                  <h3>Field {index + 1}</h3>
                  <div className="SingleItemList">
                    <div className="SingleItemListchild">
                      <label>Description:</label>
                      <input
                        type="text"
                        value={item1.description}
                        onChange={(e) =>
                          handleItemChange(index, "description", e.target.value)
                        }
                      />
                    </div>

                    <div className="SingleItemListchild">
                      <label>Amount:</label>
                      <input
                        type="number"
                        value={item1.unitPrice}
                        onChange={(e) =>
                          handleItemChange(index, "unitPrice", e.target.value)
                        }
                      />
                    </div>

                    <div>
                      <button
                        type="button"
                        style={{ marginLeft: "5px", cursor: "pointer" }}
                        value="remove"
                        className="removebtn"
                        onClick={() => {
                          removeItem(index);
                        }}
                      >
                        {" "}
                        <img src="dustbin.png" alt="" width={10} height={10} />
                      </button>
                    </div>
                  </div>
                </div>
              ))}
            </div>

            <h2
              style={{
                color: "blue",
                fontFamily: "monospace",
                fontFamily: "serif",
                marginTop: "3rem",
              }}
            >
              Deduction
            </h2>
            <div className="Item">
              <button className="itemaddbtn" onClick={addDeduction}>
                +
              </button>
              {Deduction.map((item, index) => (
                <div key={index}>
                  <h3>Field {index + 1}</h3>
                  <div className="SingleItemList">
                    <div className="SingleItemListchild">
                      <label>Description:</label>
                      <input
                        type="text"
                        value={item.description}
                        onChange={(e) =>
                          handleIDeduction(index, "description", e.target.value)
                        }
                      />
                    </div>

                    <div className="SingleItemListchild">
                      <label>Amount:</label>
                      <input
                        type="number"
                        value={item.unitPrice}
                        onChange={(e) =>
                          handleIDeduction(index, "unitPrice", e.target.value)
                        }
                      />
                    </div>

                    <div>
                      <button
                        type="button"
                        style={{ marginLeft: "5px", cursor: "pointer" }}
                        value="remove"
                        className="removebtn"
                        onClick={() => {
                          removeDeduction(index);
                        }}
                      >
                        {" "}
                        <img src="dustbin.png" alt="" width={10} height={10} />
                      </button>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default ComponentToPrint;
