import React from "react";

const Navbar = () => {
  return (
    <>
      <nav
        style={{
          background: "black",
          color: "white",
          position: "relative",
          bottom: "1rem",
        }}
      >
        <ul style={{ display: "flex" }}>
          <li style={{ listStyle: "none" }}>
            <img src="logo.png" alt="" />
          </li>
          <li
            style={{
              listStyle: "none",
              height: "4rem",
              fontSize: "x-large",
              position: "relative",
              top: "1rem",
              left: "1rem",
            }}
          >
            My Invoice Generator
          </li>
        </ul>
      </nav>
    </>
  );
};

export default Navbar;
