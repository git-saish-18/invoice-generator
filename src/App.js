import "./App.css";
import ReactToPrint from "react-to-print";
import ComponentToPrint from "../src/component/ComponentToPrint";
import { useReactToPrint } from "react-to-print";
import React, { useRef } from "react";
import Preview from "./component/Preview";
import Navbar from "./component/Navbar";
function App() {
  const componentRef = useRef();
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });
  return (
    <>
      <Navbar />
      <ComponentToPrint printcomp={componentRef} />
      {/* <Preview /> */}
    </>
  );
}

export default App;
